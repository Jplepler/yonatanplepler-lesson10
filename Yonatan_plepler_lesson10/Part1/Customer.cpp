#include "Customer.h"

//C'Tor
Customer::Customer(std::string name)
{
	_name = name;

}
//D'Tor
Customer::~Customer()
{

}



/*
This function calculates the total price of the costumer item list
Out: total price of all items
*/
double Customer::totalSum() const
{
	double sum = 0;
	std::set<Item>::iterator it;//Declare iterator of items set
	for (it = _items.begin(); it != _items.end(); ++it)//Go through each item and add it to the sum
	{
		sum += (*it).totalPrice();
	}
	return sum;
}



/*
This function adds a new item to the set of items
Or increases the count of existing item
In: the new item or the item that will be increase in amount
*/
void Customer::addItem(Item newItem) 
{
	std::set<Item>::iterator it;//Declare iterator of items set
	std::pair<std::set<Item>::iterator, bool> ret;
	ret = _items.insert(newItem);

	if (ret.second == false)//If already exists
	{
		it = ret.first;
		Item temp(*it);		//Copy of item with
		temp.setCount(temp.getCount() + 1);	//Increase amount
		_items.erase(it);	//erase old item
		_items.insert(temp);//add new item with new amount
	}

}



/*
This function removes an item from the set
In: the item that will be removed
*/
void Customer::removeItem(Item unwatedItem)
{
	bool flag = false;
	std::set<Item>::iterator it;//Declare iterator of items set
	for (it = _items.begin(); it != _items.end() && !flag; ++it)//Find item
	{
		if ((*it) == unwatedItem)//remove item
		{
			flag = true;//End loop (Set has no duplicates)
			if ((*it).getCount() == 1)
			{
				_items.erase(it);//If only one was bought then delete it
				
			}
			else//If more then one was bought then decrease count
			{
				Item temp(*it);		//Copy of item with
				temp.setCount(temp.getCount() - 1);	//Increase amount
				_items.erase(it);	//erase old item
				_items.insert(temp);//add new item with new amount
			}
			
		}
	}
}



