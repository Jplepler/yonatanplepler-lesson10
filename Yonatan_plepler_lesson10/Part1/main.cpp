#include"Customer.h"
#include "Functions.h"
#include<map>


int main()
{
	
	unsigned int choice = 0;
	string customerName = "";
	map<string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};

	do
	{
		MainFunctions::printMenu();
		cin >> choice;
		system("CLS");

		switch (choice)
		{
		case SIGN:
			MainFunctions::signIn(abcCustomers, itemList);
			break;

		case UPDATE:
			cout << "Please enter you name: ";
			cin >> customerName;
			MainFunctions::update(abcCustomers, itemList, customerName);
			break;

		case RICH:
			MainFunctions::mostExpensiveList(abcCustomers);
			break;

		case EXT:
			cout << "See you later aligator!" << endl;
			break;

		default:
			cout << "Invalid value, Please re-enter your choice" << endl;
			break;
		}


	} while (choice != EXT);



	
	return 0;
}