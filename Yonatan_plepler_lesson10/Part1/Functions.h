#pragma once
#include <iostream>
#include <map>

#define NUM_OF_ITEMS 10
#define TWO 2
#define THREE 3
using namespace std;
class Customer;
enum OPTIONS
{
	SIGN = 1,
	UPDATE,
	RICH,
	EXT
};


namespace MainFunctions
{
	void printMenu();
	void itemMenu(Item itemList[]);
	void costumerMenu();
	void signIn(map<string, Customer>& abcCustomers, Item itemList[]);
	void update(map<string, Customer>& abcCustomers, Item itemList[], string costumerName);
	void addItem(Item itemlist[], map<string, Customer>::iterator it);
	void removeItem(Item itemlist[], map<string, Customer>::iterator it);
	void mostExpensiveList(map<string, Customer>& abcCustomers);
}


