#include "Item.h"
#define SERIEL_NUMBER_LEN 5

//C'Tor
Item::Item(std::string name, std::string serielNumber, double unitPrice)
{
	this->_name = name;
	this->_serialNumber = serielNumber;
	this->_unitPrice = unitPrice;
	this->_count = 1;
}
//Copy C'Tor
Item::Item(const Item& other)
{
	_count = other.getCount();
	_unitPrice = other.getUnitPrice();
	_name = other.getName();
	_serialNumber = other.getSerialNumber();
}
//D'Tor
Item::~Item()
{

}



/*
This function calculates the sum of prices of one item
Out: amount of item item * price of item
*/
double Item::totalPrice() const
{
	return  _unitPrice * _count;
}



/*
The next 3 functions compares the seriel number of 2 items
In: 2 items
Out: true if the firsts item is smaller/bigger/equal to the second item
*/
bool Item::operator <(const Item& other) const
{
	return this->_serialNumber < other._serialNumber;
}
bool Item::operator >(const Item& other) const
{
	return this->_serialNumber > other._serialNumber;
}
bool Item::operator ==(const Item& other) const
{
	return this->_serialNumber == other._serialNumber;
}



//Getters
string Item::getName() const
{
	return _name;
}
string Item::getSerialNumber() const
{
	return _serialNumber;
}
int Item::getCount() const
{
	return _count;
}
double Item::getUnitPrice() const
{
	return _unitPrice;
}
void Item::setCount(int count)
{
	_count = count;
}



