#include "Customer.h"
#include "Functions.h"



//This Function prints the main menu
void MainFunctions::printMenu()
{
	cout << "Welcome to MagshiMart!" << endl;
	cout << "1. to sign as customer and buy items" << endl;
	cout << "2. to uptade existing customer's items" << endl;
	cout << "3. to print the customer who pays the most" << endl;
	cout << "4. to exit" << endl << endl;
}
/*
This function prints the list of items
In: array of items
*/
void MainFunctions::itemMenu(Item itemList[])
{
	int i = 0;
	cout << "The items you can buy are: (0 to exit)" << endl;
	for (i = 0; i < NUM_OF_ITEMS; i++)
	{
		//Print formatted info of item
		cout << i + 1 << ". " << itemList[i].getName() << "Price: " << itemList[i].getUnitPrice() << endl;
	}
	cout << endl;
}



//This function prints all the options for the costumer
void MainFunctions::costumerMenu()
{
	cout << "1. Add items" << endl;
	cout << "2. Remove items" << endl;
	cout << "3. Back to menu" << endl << endl;
}






/*
This function Adds a new customer (If customer already exists it returns to the main menu)
Then it adds to the customers item list whatever he want to buy
In: list of customers, list of items
*/
void MainFunctions::signIn(map<string, Customer>& abcCustomers, Item itemList[])
{
	
	map<string, Customer>::iterator it;
	string name = "";
	bool Doppelganger = false;
	unsigned int choice = 0;
	cout << "Enter your name please:" << endl;
	cin >> name;


	for (it = abcCustomers.begin(); it != abcCustomers.end(); ++it)//Check if customer already exists
	{
		if (it->first == name)
		{
			cout << "Costumer already exists! and as we all know it's impossible for 2 people to share the same name" << endl;
			Doppelganger = true;
		}
	}
	if (!Doppelganger)//if customer doesn't exist
	{
		//To Do: Create customer
		Customer newCustomer(name);
		pair<string, Customer> p(name, newCustomer);
		

		itemMenu(itemList);
		do
		{
			//Check if valid
			do
			{
				cin >> choice;
				if (choice < 0 || choice > NUM_OF_ITEMS)
				{
					cout << "Invalid choice, try again" << endl;
				}
			} while (choice < 0 || choice > NUM_OF_ITEMS);

			if (choice != 0)//if choice isn't exit
			{
				p.second.addItem(itemList[choice - 1]);//Add item
			}
		} while (choice != 0);
		abcCustomers.insert(p);//Insert new customer to list
	}
	system("CLS");
}



/*
This function adds items until user inputs 0
In: list of item user can buy, iterator that points at the customer
*/
void MainFunctions::addItem(Item itemlist[], map<string, Customer>::iterator it)
{
	int choice = 0, itemNumber = 0;
	//Get and add items
	do
	{
		MainFunctions::itemMenu(itemlist);
		//Check if valid
		do
		{
			cin >> choice;
			if (choice < EXT || choice > NUM_OF_ITEMS)
			{
				cout << "Invalid choice, try again" << endl;
			}
		} while (itemNumber < EXT || itemNumber > NUM_OF_ITEMS);

		if (itemNumber != 0)//if choice isn't exit
		{
			it->second.addItem(itemlist[itemNumber + 1]);//Add item
		}
	} while (choice != 0);
}



/*
This function removes items until user inputs 0
In: list of item user can buy, iterator that points at the customer
*/
void MainFunctions::removeItem(Item itemlist[], map<string, Customer>::iterator it)
{
	int choice = 0, itemNumber = 0;
	//Get and add items
	do
	{
		MainFunctions::itemMenu(itemlist);
		//Check if valid
		do
		{
			cin >> choice;
			if (choice < EXT || choice > NUM_OF_ITEMS)
			{
				cout << "Invalid choice, try again" << endl;
			}
		} while (itemNumber < EXT || itemNumber > NUM_OF_ITEMS);

		if (itemNumber != 0)//if choice isn't exit
		{
			it->second.removeItem(itemlist[itemNumber + 1]);//Add item
		}
	} while (choice != 0);
}



/*
This function updates an existing customer
In: map of customers, list of items the customers can buy, name of the customer that want to chagne his list of items
*/
void MainFunctions::update(map<string, Customer>& abcCustomers, Item itemList[], string costumerName)
{
	map<string, Customer>::iterator it;
	bool found = false;
	int choice = 0, itemNumber = 0, customerChoice = 0;
	


	for (it = abcCustomers.begin(); it != abcCustomers.end() && !found; ++it)//Check if customer already exists
	{
		if (it->first == costumerName)//Found costumer
		{
			found = true;
			

			do
			{
				MainFunctions::costumerMenu();//Print the menu of customer options
				//Get valid input
				do
				{
					cin >> choice;
					if (choice > THREE || choice < 1)
					{
						cout << "Invalid input, try again" << endl;
					}
				} while (choice > THREE || choice < 1);
				
				if (choice == 1)//OPT 1
				{
					MainFunctions::addItem(itemList, it);
				}
				else if (choice == TWO)// OPT 2
				{
					MainFunctions::removeItem(itemList, it);
					
				}
			} while (choice != THREE);//OPT 3



		}
	}
	if (!found)
	{
		cout << endl << "Customer has not been found! Returning to main menu." << endl;
		system("PAUSE");
	}
	system("CLS");

}



/*
This function prints the customer with the most expensive item lis
In: map of customers
*/
void MainFunctions::mostExpensiveList(map<string, Customer>& abcCustomers)
{
	double mostExpensive = 0;
	string name = "";
	map<string, Customer>::iterator it;
	if (!abcCustomers.empty())
	{
		for (it = abcCustomers.begin(); it != abcCustomers.end(); ++it)//Search for the richest customer
		{
			if (mostExpensive < (*it).second.totalSum())
			{
				mostExpensive = (*it).second.totalSum();// save total price
				name = (*it).first;//Save name
			}
		}

		cout << "Wow " << name << ", your total is the highest at " << mostExpensive << " ILS. leave some for the rest of us!" << endl;
	}
	else//No customers
	{
		cout << "No customers so far, please bring some in before we go bankrupt );" << endl;
	}
	system("PAUSE");
	system("CLS");
}